﻿
namespace calculator
{
    partial class calculator
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.display = new System.Windows.Forms.Label();
            this.basicOperations = new System.Windows.Forms.TableLayoutPanel();
            this.comma = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.plus_minus = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.percent = new System.Windows.Forms.Button();
            this.bracket = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.equal = new System.Windows.Forms.Button();
            this.advancedFunctions = new System.Windows.Forms.TableLayoutPanel();
            this.sinnus = new System.Windows.Forms.Button();
            this.cossinus = new System.Windows.Forms.Button();
            this.tangens = new System.Windows.Forms.Button();
            this.sqr = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.logarithm = new System.Windows.Forms.Button();
            this.codeRights = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mc = new System.Windows.Forms.Button();
            this.mr = new System.Windows.Forms.Button();
            this.m_plus = new System.Windows.Forms.Button();
            this.m_minus = new System.Windows.Forms.Button();
            this.displayTop = new System.Windows.Forms.Label();
            this.basicOperations.SuspendLayout();
            this.advancedFunctions.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // display
            // 
            this.display.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.display.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.display.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.display.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.display.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.display.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.display.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.display.Location = new System.Drawing.Point(11, 69);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(447, 88);
            this.display.TabIndex = 0;
            this.display.Text = "0";
            this.display.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // basicOperations
            // 
            this.basicOperations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.basicOperations.ColumnCount = 4;
            this.basicOperations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.basicOperations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.basicOperations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.basicOperations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.basicOperations.Controls.Add(this.comma, 2, 4);
            this.basicOperations.Controls.Add(this.zero, 1, 4);
            this.basicOperations.Controls.Add(this.plus_minus, 0, 4);
            this.basicOperations.Controls.Add(this.plus, 3, 3);
            this.basicOperations.Controls.Add(this.three, 2, 3);
            this.basicOperations.Controls.Add(this.two, 1, 3);
            this.basicOperations.Controls.Add(this.one, 0, 3);
            this.basicOperations.Controls.Add(this.minus, 3, 2);
            this.basicOperations.Controls.Add(this.six, 2, 2);
            this.basicOperations.Controls.Add(this.five, 1, 2);
            this.basicOperations.Controls.Add(this.four, 0, 2);
            this.basicOperations.Controls.Add(this.multiply, 3, 1);
            this.basicOperations.Controls.Add(this.nine, 2, 1);
            this.basicOperations.Controls.Add(this.eight, 1, 1);
            this.basicOperations.Controls.Add(this.seven, 0, 1);
            this.basicOperations.Controls.Add(this.divide, 3, 0);
            this.basicOperations.Controls.Add(this.percent, 2, 0);
            this.basicOperations.Controls.Add(this.bracket, 1, 0);
            this.basicOperations.Controls.Add(this.cancel, 0, 0);
            this.basicOperations.Controls.Add(this.equal, 3, 4);
            this.basicOperations.Location = new System.Drawing.Point(14, 261);
            this.basicOperations.Name = "basicOperations";
            this.basicOperations.RowCount = 5;
            this.basicOperations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.basicOperations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.basicOperations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.basicOperations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.basicOperations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.basicOperations.Size = new System.Drawing.Size(447, 296);
            this.basicOperations.TabIndex = 1;
            // 
            // comma
            // 
            this.comma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comma.BackColor = System.Drawing.SystemColors.ControlDark;
            this.comma.FlatAppearance.BorderSize = 0;
            this.comma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comma.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.comma.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comma.Location = new System.Drawing.Point(223, 237);
            this.comma.Margin = new System.Windows.Forms.Padding(1);
            this.comma.Name = "comma";
            this.comma.Padding = new System.Windows.Forms.Padding(1);
            this.comma.Size = new System.Drawing.Size(109, 58);
            this.comma.TabIndex = 19;
            this.comma.Text = ",";
            this.comma.UseVisualStyleBackColor = false;
            this.comma.Click += new System.EventHandler(this.comma_Click);
            // 
            // zero
            // 
            this.zero.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zero.BackColor = System.Drawing.SystemColors.ControlDark;
            this.zero.FlatAppearance.BorderSize = 0;
            this.zero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zero.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.zero.ForeColor = System.Drawing.SystemColors.ControlText;
            this.zero.Location = new System.Drawing.Point(112, 237);
            this.zero.Margin = new System.Windows.Forms.Padding(1);
            this.zero.Name = "zero";
            this.zero.Padding = new System.Windows.Forms.Padding(1);
            this.zero.Size = new System.Drawing.Size(109, 58);
            this.zero.TabIndex = 18;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = false;
            this.zero.Click += new System.EventHandler(this.number_Click);
            // 
            // plus_minus
            // 
            this.plus_minus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plus_minus.BackColor = System.Drawing.SystemColors.ControlDark;
            this.plus_minus.FlatAppearance.BorderSize = 0;
            this.plus_minus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plus_minus.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.plus_minus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.plus_minus.Location = new System.Drawing.Point(1, 237);
            this.plus_minus.Margin = new System.Windows.Forms.Padding(1);
            this.plus_minus.Name = "plus_minus";
            this.plus_minus.Padding = new System.Windows.Forms.Padding(1);
            this.plus_minus.Size = new System.Drawing.Size(109, 58);
            this.plus_minus.TabIndex = 17;
            this.plus_minus.Text = "±";
            this.plus_minus.UseVisualStyleBackColor = false;
            this.plus_minus.Click += new System.EventHandler(this.plus_minus_Click);
            // 
            // plus
            // 
            this.plus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plus.BackColor = System.Drawing.Color.SeaGreen;
            this.plus.FlatAppearance.BorderSize = 0;
            this.plus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plus.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.plus.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.plus.Location = new System.Drawing.Point(334, 178);
            this.plus.Margin = new System.Windows.Forms.Padding(1);
            this.plus.Name = "plus";
            this.plus.Padding = new System.Windows.Forms.Padding(1);
            this.plus.Size = new System.Drawing.Size(112, 57);
            this.plus.TabIndex = 16;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = false;
            this.plus.Click += new System.EventHandler(this.mathOp_Click);
            // 
            // three
            // 
            this.three.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.three.BackColor = System.Drawing.SystemColors.ControlDark;
            this.three.FlatAppearance.BorderSize = 0;
            this.three.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.three.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.three.ForeColor = System.Drawing.SystemColors.ControlText;
            this.three.Location = new System.Drawing.Point(223, 178);
            this.three.Margin = new System.Windows.Forms.Padding(1);
            this.three.Name = "three";
            this.three.Padding = new System.Windows.Forms.Padding(1);
            this.three.Size = new System.Drawing.Size(109, 57);
            this.three.TabIndex = 15;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = false;
            this.three.Click += new System.EventHandler(this.number_Click);
            // 
            // two
            // 
            this.two.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.two.BackColor = System.Drawing.SystemColors.ControlDark;
            this.two.FlatAppearance.BorderSize = 0;
            this.two.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.two.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.two.ForeColor = System.Drawing.SystemColors.ControlText;
            this.two.Location = new System.Drawing.Point(112, 178);
            this.two.Margin = new System.Windows.Forms.Padding(1);
            this.two.Name = "two";
            this.two.Padding = new System.Windows.Forms.Padding(1);
            this.two.Size = new System.Drawing.Size(109, 57);
            this.two.TabIndex = 14;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = false;
            this.two.Click += new System.EventHandler(this.number_Click);
            // 
            // one
            // 
            this.one.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.one.BackColor = System.Drawing.SystemColors.ControlDark;
            this.one.FlatAppearance.BorderSize = 0;
            this.one.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.one.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.one.ForeColor = System.Drawing.SystemColors.ControlText;
            this.one.Location = new System.Drawing.Point(1, 178);
            this.one.Margin = new System.Windows.Forms.Padding(1);
            this.one.Name = "one";
            this.one.Padding = new System.Windows.Forms.Padding(1);
            this.one.Size = new System.Drawing.Size(109, 57);
            this.one.TabIndex = 13;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = false;
            this.one.Click += new System.EventHandler(this.number_Click);
            // 
            // minus
            // 
            this.minus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.minus.BackColor = System.Drawing.Color.SeaGreen;
            this.minus.FlatAppearance.BorderSize = 0;
            this.minus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.minus.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.minus.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.minus.Location = new System.Drawing.Point(334, 119);
            this.minus.Margin = new System.Windows.Forms.Padding(1);
            this.minus.Name = "minus";
            this.minus.Padding = new System.Windows.Forms.Padding(1);
            this.minus.Size = new System.Drawing.Size(112, 57);
            this.minus.TabIndex = 12;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = false;
            this.minus.Click += new System.EventHandler(this.mathOp_Click);
            // 
            // six
            // 
            this.six.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.six.BackColor = System.Drawing.SystemColors.ControlDark;
            this.six.FlatAppearance.BorderSize = 0;
            this.six.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.six.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.six.ForeColor = System.Drawing.SystemColors.ControlText;
            this.six.Location = new System.Drawing.Point(223, 119);
            this.six.Margin = new System.Windows.Forms.Padding(1);
            this.six.Name = "six";
            this.six.Padding = new System.Windows.Forms.Padding(1);
            this.six.Size = new System.Drawing.Size(109, 57);
            this.six.TabIndex = 11;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = false;
            this.six.Click += new System.EventHandler(this.number_Click);
            // 
            // five
            // 
            this.five.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.five.BackColor = System.Drawing.SystemColors.ControlDark;
            this.five.FlatAppearance.BorderSize = 0;
            this.five.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.five.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.five.ForeColor = System.Drawing.SystemColors.ControlText;
            this.five.Location = new System.Drawing.Point(112, 119);
            this.five.Margin = new System.Windows.Forms.Padding(1);
            this.five.Name = "five";
            this.five.Padding = new System.Windows.Forms.Padding(1);
            this.five.Size = new System.Drawing.Size(109, 57);
            this.five.TabIndex = 10;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = false;
            this.five.Click += new System.EventHandler(this.number_Click);
            // 
            // four
            // 
            this.four.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.four.BackColor = System.Drawing.SystemColors.ControlDark;
            this.four.FlatAppearance.BorderSize = 0;
            this.four.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.four.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.four.ForeColor = System.Drawing.SystemColors.ControlText;
            this.four.Location = new System.Drawing.Point(1, 119);
            this.four.Margin = new System.Windows.Forms.Padding(1);
            this.four.Name = "four";
            this.four.Padding = new System.Windows.Forms.Padding(1);
            this.four.Size = new System.Drawing.Size(109, 57);
            this.four.TabIndex = 9;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = false;
            this.four.Click += new System.EventHandler(this.number_Click);
            // 
            // multiply
            // 
            this.multiply.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.multiply.BackColor = System.Drawing.Color.SeaGreen;
            this.multiply.FlatAppearance.BorderSize = 0;
            this.multiply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.multiply.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.multiply.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.multiply.Location = new System.Drawing.Point(334, 60);
            this.multiply.Margin = new System.Windows.Forms.Padding(1);
            this.multiply.Name = "multiply";
            this.multiply.Padding = new System.Windows.Forms.Padding(1);
            this.multiply.Size = new System.Drawing.Size(112, 57);
            this.multiply.TabIndex = 8;
            this.multiply.Text = "x";
            this.multiply.UseVisualStyleBackColor = false;
            this.multiply.Click += new System.EventHandler(this.mathOp_Click);
            // 
            // nine
            // 
            this.nine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nine.BackColor = System.Drawing.SystemColors.ControlDark;
            this.nine.FlatAppearance.BorderSize = 0;
            this.nine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nine.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.nine.ForeColor = System.Drawing.SystemColors.ControlText;
            this.nine.Location = new System.Drawing.Point(223, 60);
            this.nine.Margin = new System.Windows.Forms.Padding(1);
            this.nine.Name = "nine";
            this.nine.Padding = new System.Windows.Forms.Padding(1);
            this.nine.Size = new System.Drawing.Size(109, 57);
            this.nine.TabIndex = 7;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = false;
            this.nine.Click += new System.EventHandler(this.number_Click);
            // 
            // eight
            // 
            this.eight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eight.BackColor = System.Drawing.SystemColors.ControlDark;
            this.eight.FlatAppearance.BorderSize = 0;
            this.eight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eight.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.eight.ForeColor = System.Drawing.SystemColors.ControlText;
            this.eight.Location = new System.Drawing.Point(112, 60);
            this.eight.Margin = new System.Windows.Forms.Padding(1);
            this.eight.Name = "eight";
            this.eight.Padding = new System.Windows.Forms.Padding(1);
            this.eight.Size = new System.Drawing.Size(109, 57);
            this.eight.TabIndex = 6;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = false;
            this.eight.Click += new System.EventHandler(this.number_Click);
            // 
            // seven
            // 
            this.seven.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seven.BackColor = System.Drawing.SystemColors.ControlDark;
            this.seven.FlatAppearance.BorderSize = 0;
            this.seven.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seven.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold);
            this.seven.ForeColor = System.Drawing.SystemColors.ControlText;
            this.seven.Location = new System.Drawing.Point(1, 60);
            this.seven.Margin = new System.Windows.Forms.Padding(1);
            this.seven.Name = "seven";
            this.seven.Padding = new System.Windows.Forms.Padding(1);
            this.seven.Size = new System.Drawing.Size(109, 57);
            this.seven.TabIndex = 5;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = false;
            this.seven.Click += new System.EventHandler(this.number_Click);
            // 
            // divide
            // 
            this.divide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.divide.BackColor = System.Drawing.Color.SeaGreen;
            this.divide.FlatAppearance.BorderSize = 0;
            this.divide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.divide.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.divide.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.divide.Location = new System.Drawing.Point(334, 1);
            this.divide.Margin = new System.Windows.Forms.Padding(1);
            this.divide.Name = "divide";
            this.divide.Padding = new System.Windows.Forms.Padding(1);
            this.divide.Size = new System.Drawing.Size(112, 57);
            this.divide.TabIndex = 4;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = false;
            this.divide.Click += new System.EventHandler(this.mathOp_Click);
            // 
            // percent
            // 
            this.percent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.percent.BackColor = System.Drawing.Color.SeaGreen;
            this.percent.FlatAppearance.BorderSize = 0;
            this.percent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.percent.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.percent.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.percent.Location = new System.Drawing.Point(223, 1);
            this.percent.Margin = new System.Windows.Forms.Padding(1);
            this.percent.Name = "percent";
            this.percent.Padding = new System.Windows.Forms.Padding(1);
            this.percent.Size = new System.Drawing.Size(109, 57);
            this.percent.TabIndex = 3;
            this.percent.Text = "%";
            this.percent.UseVisualStyleBackColor = false;
            this.percent.Click += new System.EventHandler(this.percent_Click);
            // 
            // bracket
            // 
            this.bracket.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bracket.BackColor = System.Drawing.Color.SeaGreen;
            this.bracket.FlatAppearance.BorderSize = 0;
            this.bracket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bracket.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.bracket.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.bracket.Location = new System.Drawing.Point(112, 1);
            this.bracket.Margin = new System.Windows.Forms.Padding(1);
            this.bracket.Name = "bracket";
            this.bracket.Padding = new System.Windows.Forms.Padding(1);
            this.bracket.Size = new System.Drawing.Size(109, 57);
            this.bracket.TabIndex = 2;
            this.bracket.Text = "( )";
            this.bracket.UseVisualStyleBackColor = false;
            this.bracket.Click += new System.EventHandler(this.bracket_Click);
            // 
            // cancel
            // 
            this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancel.BackColor = System.Drawing.Color.DarkRed;
            this.cancel.FlatAppearance.BorderSize = 0;
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.cancel.ForeColor = System.Drawing.Color.IndianRed;
            this.cancel.Location = new System.Drawing.Point(1, 1);
            this.cancel.Margin = new System.Windows.Forms.Padding(1);
            this.cancel.Name = "cancel";
            this.cancel.Padding = new System.Windows.Forms.Padding(1);
            this.cancel.Size = new System.Drawing.Size(109, 57);
            this.cancel.TabIndex = 1;
            this.cancel.Text = "C";
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // equal
            // 
            this.equal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equal.BackColor = System.Drawing.Color.PaleGreen;
            this.equal.FlatAppearance.BorderSize = 0;
            this.equal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.equal.Font = new System.Drawing.Font("Segoe UI Semibold", 21F, System.Drawing.FontStyle.Bold);
            this.equal.ForeColor = System.Drawing.Color.SeaGreen;
            this.equal.Location = new System.Drawing.Point(334, 237);
            this.equal.Margin = new System.Windows.Forms.Padding(1);
            this.equal.Name = "equal";
            this.equal.Padding = new System.Windows.Forms.Padding(1);
            this.equal.Size = new System.Drawing.Size(112, 58);
            this.equal.TabIndex = 0;
            this.equal.Text = "=";
            this.equal.UseVisualStyleBackColor = false;
            this.equal.Click += new System.EventHandler(this.equal_Click);
            // 
            // advancedFunctions
            // 
            this.advancedFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.advancedFunctions.ColumnCount = 6;
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.advancedFunctions.Controls.Add(this.sinnus, 0, 0);
            this.advancedFunctions.Controls.Add(this.cossinus, 0, 0);
            this.advancedFunctions.Controls.Add(this.tangens, 0, 0);
            this.advancedFunctions.Controls.Add(this.sqr, 0, 0);
            this.advancedFunctions.Controls.Add(this.sqrt, 0, 0);
            this.advancedFunctions.Controls.Add(this.logarithm, 0, 0);
            this.advancedFunctions.Location = new System.Drawing.Point(13, 207);
            this.advancedFunctions.Name = "advancedFunctions";
            this.advancedFunctions.RowCount = 1;
            this.advancedFunctions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.advancedFunctions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.advancedFunctions.Size = new System.Drawing.Size(447, 48);
            this.advancedFunctions.TabIndex = 2;
            // 
            // sinnus
            // 
            this.sinnus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sinnus.BackColor = System.Drawing.SystemColors.GrayText;
            this.sinnus.FlatAppearance.BorderSize = 0;
            this.sinnus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sinnus.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.sinnus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.sinnus.Location = new System.Drawing.Point(223, 1);
            this.sinnus.Margin = new System.Windows.Forms.Padding(1);
            this.sinnus.Name = "sinnus";
            this.sinnus.Padding = new System.Windows.Forms.Padding(1);
            this.sinnus.Size = new System.Drawing.Size(72, 46);
            this.sinnus.TabIndex = 13;
            this.sinnus.Text = "sin";
            this.sinnus.UseVisualStyleBackColor = false;
            this.sinnus.Click += new System.EventHandler(this.sinnus_Click);
            // 
            // cossinus
            // 
            this.cossinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cossinus.BackColor = System.Drawing.SystemColors.GrayText;
            this.cossinus.FlatAppearance.BorderSize = 0;
            this.cossinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cossinus.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.cossinus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cossinus.Location = new System.Drawing.Point(297, 1);
            this.cossinus.Margin = new System.Windows.Forms.Padding(1);
            this.cossinus.Name = "cossinus";
            this.cossinus.Padding = new System.Windows.Forms.Padding(1);
            this.cossinus.Size = new System.Drawing.Size(72, 46);
            this.cossinus.TabIndex = 12;
            this.cossinus.Text = "cos";
            this.cossinus.UseVisualStyleBackColor = false;
            this.cossinus.Click += new System.EventHandler(this.cosinnus_Click);
            // 
            // tangens
            // 
            this.tangens.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tangens.BackColor = System.Drawing.SystemColors.GrayText;
            this.tangens.FlatAppearance.BorderSize = 0;
            this.tangens.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tangens.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.tangens.ForeColor = System.Drawing.SystemColors.Desktop;
            this.tangens.Location = new System.Drawing.Point(371, 1);
            this.tangens.Margin = new System.Windows.Forms.Padding(1);
            this.tangens.Name = "tangens";
            this.tangens.Padding = new System.Windows.Forms.Padding(1);
            this.tangens.Size = new System.Drawing.Size(75, 46);
            this.tangens.TabIndex = 11;
            this.tangens.Text = "tan";
            this.tangens.UseVisualStyleBackColor = false;
            this.tangens.Click += new System.EventHandler(this.tangens_Click);
            // 
            // sqr
            // 
            this.sqr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sqr.BackColor = System.Drawing.SystemColors.GrayText;
            this.sqr.FlatAppearance.BorderSize = 0;
            this.sqr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sqr.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.sqr.ForeColor = System.Drawing.SystemColors.Desktop;
            this.sqr.Location = new System.Drawing.Point(1, 1);
            this.sqr.Margin = new System.Windows.Forms.Padding(1);
            this.sqr.Name = "sqr";
            this.sqr.Padding = new System.Windows.Forms.Padding(1);
            this.sqr.Size = new System.Drawing.Size(72, 46);
            this.sqr.TabIndex = 10;
            this.sqr.Text = "x2";
            this.sqr.UseVisualStyleBackColor = false;
            this.sqr.Click += new System.EventHandler(this.sqr_Click);
            // 
            // sqrt
            // 
            this.sqrt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sqrt.BackColor = System.Drawing.SystemColors.GrayText;
            this.sqrt.FlatAppearance.BorderSize = 0;
            this.sqrt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sqrt.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.sqrt.ForeColor = System.Drawing.SystemColors.Desktop;
            this.sqrt.Location = new System.Drawing.Point(75, 1);
            this.sqrt.Margin = new System.Windows.Forms.Padding(1);
            this.sqrt.Name = "sqrt";
            this.sqrt.Padding = new System.Windows.Forms.Padding(1);
            this.sqrt.Size = new System.Drawing.Size(72, 46);
            this.sqrt.TabIndex = 9;
            this.sqrt.Text = "√";
            this.sqrt.UseVisualStyleBackColor = false;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // logarithm
            // 
            this.logarithm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logarithm.BackColor = System.Drawing.SystemColors.GrayText;
            this.logarithm.FlatAppearance.BorderSize = 0;
            this.logarithm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logarithm.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.logarithm.ForeColor = System.Drawing.SystemColors.Desktop;
            this.logarithm.Location = new System.Drawing.Point(149, 1);
            this.logarithm.Margin = new System.Windows.Forms.Padding(1);
            this.logarithm.Name = "logarithm";
            this.logarithm.Padding = new System.Windows.Forms.Padding(1);
            this.logarithm.Size = new System.Drawing.Size(72, 46);
            this.logarithm.TabIndex = 8;
            this.logarithm.Text = "log";
            this.logarithm.UseVisualStyleBackColor = false;
            this.logarithm.Click += new System.EventHandler(this.logarithm_Click);
            // 
            // codeRights
            // 
            this.codeRights.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.codeRights.AutoSize = true;
            this.codeRights.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.codeRights.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.codeRights.Location = new System.Drawing.Point(265, 572);
            this.codeRights.Name = "codeRights";
            this.codeRights.Size = new System.Drawing.Size(196, 13);
            this.codeRights.TabIndex = 3;
            this.codeRights.Text = "coded by Mikolaj Niedbala | 2021";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Controls.Add(this.mc, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mr, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.m_plus, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.m_minus, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 160);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(447, 41);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // mc
            // 
            this.mc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mc.FlatAppearance.BorderSize = 0;
            this.mc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mc.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.mc.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.mc.Location = new System.Drawing.Point(223, 1);
            this.mc.Margin = new System.Windows.Forms.Padding(1);
            this.mc.Name = "mc";
            this.mc.Padding = new System.Windows.Forms.Padding(1);
            this.mc.Size = new System.Drawing.Size(109, 39);
            this.mc.TabIndex = 13;
            this.mc.Text = "MC";
            this.mc.UseVisualStyleBackColor = false;
            this.mc.Click += new System.EventHandler(this.mc_Click);
            // 
            // mr
            // 
            this.mr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mr.FlatAppearance.BorderSize = 0;
            this.mr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mr.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.mr.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.mr.Location = new System.Drawing.Point(334, 1);
            this.mr.Margin = new System.Windows.Forms.Padding(1);
            this.mr.Name = "mr";
            this.mr.Padding = new System.Windows.Forms.Padding(1);
            this.mr.Size = new System.Drawing.Size(112, 39);
            this.mr.TabIndex = 10;
            this.mr.Text = "MR";
            this.mr.UseVisualStyleBackColor = false;
            this.mr.Click += new System.EventHandler(this.mr_Click);
            // 
            // m_plus
            // 
            this.m_plus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_plus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_plus.FlatAppearance.BorderSize = 0;
            this.m_plus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_plus.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.m_plus.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.m_plus.Location = new System.Drawing.Point(1, 1);
            this.m_plus.Margin = new System.Windows.Forms.Padding(1);
            this.m_plus.Name = "m_plus";
            this.m_plus.Padding = new System.Windows.Forms.Padding(1);
            this.m_plus.Size = new System.Drawing.Size(109, 39);
            this.m_plus.TabIndex = 9;
            this.m_plus.Text = "M+";
            this.m_plus.UseVisualStyleBackColor = false;
            this.m_plus.Click += new System.EventHandler(this.m_plus_Click);
            // 
            // m_minus
            // 
            this.m_minus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_minus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_minus.FlatAppearance.BorderSize = 0;
            this.m_minus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_minus.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.m_minus.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.m_minus.Location = new System.Drawing.Point(112, 1);
            this.m_minus.Margin = new System.Windows.Forms.Padding(1);
            this.m_minus.Name = "m_minus";
            this.m_minus.Padding = new System.Windows.Forms.Padding(1);
            this.m_minus.Size = new System.Drawing.Size(109, 39);
            this.m_minus.TabIndex = 8;
            this.m_minus.Text = "M-";
            this.m_minus.UseVisualStyleBackColor = false;
            this.m_minus.Click += new System.EventHandler(this.m_minus_Click);
            // 
            // displayTop
            // 
            this.displayTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.displayTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.displayTop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayTop.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.displayTop.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.displayTop.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.displayTop.Location = new System.Drawing.Point(14, 13);
            this.displayTop.Name = "displayTop";
            this.displayTop.Size = new System.Drawing.Size(444, 49);
            this.displayTop.TabIndex = 5;
            this.displayTop.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // calculator
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(471, 606);
            this.Controls.Add(this.displayTop);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.codeRights);
            this.Controls.Add(this.advancedFunctions);
            this.Controls.Add(this.basicOperations);
            this.Controls.Add(this.display);
            this.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "calculator";
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.calculator_Load);
            this.basicOperations.ResumeLayout(false);
            this.advancedFunctions.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label display;
        private System.Windows.Forms.TableLayoutPanel basicOperations;
        private System.Windows.Forms.Button equal;
        private System.Windows.Forms.Button comma;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button plus_minus;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button percent;
        private System.Windows.Forms.Button bracket;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.TableLayoutPanel advancedFunctions;
        private System.Windows.Forms.Button logarithm;
        private System.Windows.Forms.Button sinnus;
        private System.Windows.Forms.Button cossinus;
        private System.Windows.Forms.Button tangens;
        private System.Windows.Forms.Button sqr;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Label codeRights;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button mc;
        private System.Windows.Forms.Button mr;
        private System.Windows.Forms.Button m_plus;
        private System.Windows.Forms.Button m_minus;
        private System.Windows.Forms.Label displayTop;
    }
}

