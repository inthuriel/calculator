﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Display
    {
        /// <summary>
        /// Prepares output for second line of calculator display
        /// </summary>
        /// <param name="elements">List of elements aka. equasion</param>
        /// <returns>string</returns>
        public string updateScondLineDisplay(List<string> elements)
        {
            string displayOperations = "";

            foreach (string element in elements)
            {
                displayOperations += String.Format("{0} ", element);
            }

            return displayOperations;
        }
    }
}
