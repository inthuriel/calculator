﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public class Mathematics
    {

        /// <summary>
        /// Return result of mathematic operations calculated basing on List<> input
        /// </summary>
        /// <param name="elements">List of elements aka. equasion</param>
        /// <returns>double</returns>
        public double result(List<string> elements) {
            double result = 0;
            bool do_further = false;

            // process brackets if we have any
            do
            {
                int bracketStart = elements.FindLastIndex(element => element == "(");
                int bracketEnd = elements.FindIndex(element => element == ")");

                if (bracketStart != -1)
                {
                    int index = bracketStart + 1;
                    int count = bracketEnd - bracketStart - 1;
                    List<string> mathematicsBetweenBrackets = elements.GetRange(index, count);

                    try
                    {
                        result = process_mathematical_operations(mathematicsBetweenBrackets);
                    }
                    catch (DivideByZeroException e)
                    {
                        throw new DivideByZeroException(e.Message);
                    }

                    elements[bracketStart] = result.ToString();
                    elements.RemoveRange(index, count + 1);

                    do_further = true;
                }
                else
                {
                    do_further = false;
                }

            } while (do_further);

            // process normalized obeject - without brackets, with basic operations only
            try
            {
                result = process_mathematical_operations(elements);
            }
            catch (DivideByZeroException e)
            {
                throw new DivideByZeroException(e.Message);
            }

            return result;
        }

        /// <summary>
        /// Process and parse input List<> to perform mathematic operations inc. operations queue
        /// </summary>
        /// <param name="elements">List of elements aka. equasion</param>
        /// <returns>double</returns>
        private double process_mathematical_operations(List<string> elements)
        {
            double score = 0;
            bool do_further = false;

            // operations queue may be hardcoded as it's static ;) 
            // do multiply
            do
            {
                int index = elements.FindIndex(element => element == "x");

                if (index != -1)
                {
                    double number1 = process_extra_mathematical_operations(elements.ElementAt(index - 1));
                    double number2 = process_extra_mathematical_operations(elements.ElementAt(index + 1));
                    double result = number1 * number2;
                    elements[index - 1] = result.ToString();
                    elements.RemoveAt(index + 1);
                    elements.RemoveAt(index);
                    do_further = true;
                }
                else
                {
                    do_further = false;
                }
            } while (do_further);

            //do divide
            do
            {
                int index = elements.FindIndex(element => element == "/");

                if (index != -1)
                {
                    double number1 = process_extra_mathematical_operations(elements.ElementAt(index - 1));
                    double number2 = process_extra_mathematical_operations(elements.ElementAt(index + 1));

                    if (number2.Equals(0))
                    {
                        throw new DivideByZeroException("You can't divide by zero.");
                    }

                    double result = number1 / number2;
                    elements[index - 1] = result.ToString();
                    elements.RemoveAt(index + 1);
                    elements.RemoveAt(index);
                    do_further = true;
                }
                else
                {
                    do_further = false;
                }
            } while (do_further);

            //do subtraction
            do
            {
                int index = elements.FindIndex(element => element == "-");

                if (index != -1)
                {
                    double number1 = process_extra_mathematical_operations(elements.ElementAt(index - 1));
                    double number2 = process_extra_mathematical_operations(elements.ElementAt(index + 1));
                    double result = number1 - number2;
                    elements[index - 1] = result.ToString();
                    elements.RemoveAt(index + 1);
                    elements.RemoveAt(index);
                    do_further = true;
                }
                else
                {
                    do_further = false;
                }
            } while (do_further);

            // do addition
            do
            {
                int index = elements.FindIndex(element => element == "+");

                if (index != -1)
                {
                    double number1 = process_extra_mathematical_operations(elements.ElementAt(index - 1));
                    double number2 = process_extra_mathematical_operations(elements.ElementAt(index + 1));
                    double result = number1 + number2;
                    elements[index - 1] = result.ToString();
                    elements.RemoveAt(index + 1);
                    elements.RemoveAt(index);
                    do_further = true;
                }
                else
                {
                    do_further = false;
                }
            } while (do_further);

            score = process_extra_mathematical_operations(elements.ElementAt(0));

            return score;
        }

        /// <summary>
        /// Perform advanced mathematical operations eg. log, cos etc.
        /// </summary>
        /// <param name="number">number casted to string</param>
        /// <returns>double</returns>
        private double process_extra_mathematical_operations(string number) {
            string sqr = @"sqr\((\d+)\)";
            string sqrt = @"√\((\d+)\)";
            string log = @"log\((\d+)\)";
            string sin = @"sin\((\d+)\)";
            string cos = @"cos\((\d+)\)";
            string tan = @"tan\((\d+)\)";
            double result = 0;

            if (number.StartsWith("sqr"))
            {
                var matches = Regex.Matches(number, sqr);
                number = matches[0].Groups[1].Value;
                result = Math.Pow(double.Parse(number), 2);
            }
            else if (number.StartsWith("√"))
            {
                var matches = Regex.Matches(number, sqrt);
                number = matches[0].Groups[1].Value;
                result = Math.Sqrt(double.Parse(number));
            }
            else if (number.StartsWith("log"))
            {
                var matches = Regex.Matches(number, log);
                number = matches[0].Groups[1].Value;
                result = Math.Log(double.Parse(number));
            }
            else if (number.StartsWith("sin"))
            {
                var matches = Regex.Matches(number, sin);
                number = matches[0].Groups[1].Value;
                result = Math.Sin(double.Parse(number));
            }
            else if (number.StartsWith("cos"))
            {
                var matches = Regex.Matches(number, cos);
                number = matches[0].Groups[1].Value;
                result = Math.Cos(double.Parse(number));
            }
            else if (number.StartsWith("tan"))
            {
                var matches = Regex.Matches(number, tan);
                number = matches[0].Groups[1].Value;
                result = Math.Tan(double.Parse(number));
            }
            else
            {
                result = double.Parse(number);
            }

            return result;
        }
    }
}
