﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class calculator : Form
    {
        private double currentNumber;
        private bool isResult;
        private bool isBracketOpen;
        private int numberOfBracketsOpen;
        private double memoryValue;
        private List<string> elements = new List<string>();
        private Mathematics math = new Mathematics();
        private Display displayOperations = new Display();

        public calculator()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads calculator form, sets defaults
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">exception</param>
        private void calculator_Load(object sender, EventArgs e)
        {
            currentNumber = 0;
            numberOfBracketsOpen = 0;
            display.Text = "0";

            isResult = false;
            isBracketOpen = false;
            mc.Enabled = false;
            mr.Enabled = false;
        }

        /// <summary>
        /// Function handler for click number button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void number_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            string number = "";
            string last = String.IsNullOrEmpty(elements.LastOrDefault()) ? "" : elements.LastOrDefault();

            if(last == "(")
                isBracketOpen = true;

            if (!isResult)
            {
                number = display.Text;
                isResult = false;
            }

            // max display lenght is 14 for this project
            if (display.Text.Length < 14)
            {
                currentNumber = double.Parse(number += button.Text);
                display.Text = currentNumber.ToString();
            }
        }

        /// <summary>
        /// Function handler for click advanced math functions button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void mathOp_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            string last = String.IsNullOrEmpty(elements.LastOrDefault()) ? "" : elements.LastOrDefault();

            if (!last.StartsWith("sqr") && 
                !last.StartsWith("√") && 
                !last.StartsWith("log") &&
                !last.StartsWith("sin") && 
                !last.StartsWith("cos") &&
                !last.StartsWith("tan") &&
                last != ")")
                elements.Add(currentNumber.ToString());
            elements.Add(button.Text);

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();

        }

        /// <summary>
        /// Function handler for click brackets button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>        
        private void bracket_Click(object sender, EventArgs e)
        {
            string last = String.IsNullOrEmpty(elements.LastOrDefault()) ? "" : elements.LastOrDefault();

            if ((Regex.IsMatch(last, @"\d") || last == "") && (elements.Count == 0 && display.Text != "0"))
            {
                elements.Add(display.Text);
                elements.Add("x");
            }

            if (!isBracketOpen)
            {
                elements.Add("(");
                numberOfBracketsOpen++;
            }                
            else
            {
                if (numberOfBracketsOpen > 0)
                {
                    Console.WriteLine(last);
                    if (!last.EndsWith(")"))
                        elements.Add(display.Text);
                    elements.Add(")");

                    numberOfBracketsOpen--;
                    if(numberOfBracketsOpen != 0)
                        isBracketOpen = true;
                }
            }


            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = double.Parse(display.Text);

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click cancel button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void cancel_Click(object sender, EventArgs e)
        {
            display.Text = "0";
            displayTop.Text = "";
            currentNumber = 0;
            isBracketOpen = false;
            numberOfBracketsOpen = 0;
            elements.Clear();
        }

        /// <summary>
        /// Function handler for click equal button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void equal_Click(object sender, EventArgs e)
        {
            string[] mathElements = { "+", "-", "/", "*" };
            double score;

            string last = String.IsNullOrEmpty(elements.LastOrDefault()) ? "" : elements.LastOrDefault();

            if (last != ")")
            {
                elements.Add(currentNumber.ToString());
                currentNumber = double.Parse(display.Text);
            }

            last = String.IsNullOrEmpty(elements.LastOrDefault()) ? "" : elements.LastOrDefault();

            if (currentNumber == 0 && elements.Count > 1 && mathElements.Contains(last))
                    elements.RemoveAt(elements.Count-1);


            try
            {
                score = math.result(elements);
                displayTop.Text = score.ToString();
            }
            catch (DivideByZeroException exception)
            {
                displayTop.Text = exception.Message;
            }

            currentNumber = 0;
            elements.Clear();

            isResult = true;

        }

        /// <summary>
        /// Function handler for click +/- button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void plus_minus_Click(object sender, EventArgs e)
        {
            // avoid -0
            if (currentNumber != 0)
            {
                if (display.Text.StartsWith("-"))
                {
                    display.Text = display.Text.Substring(1);
                }
                else
                {
                    display.Text = "-" + display.Text;
                }
                currentNumber = double.Parse(display.Text);
            }
        }

        /// <summary>
        /// Function handler for click % button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void percent_Click(object sender, EventArgs e)
        {
            double number = double.Parse(elements.ElementAt(elements.Count - 2));
            double percent = double.Parse(display.Text) / 100;
            double result = number * percent;

            elements.Add(result.ToString());

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();

        }

        /// <summary>
        /// Function handler for click comma button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void comma_Click(object sender, EventArgs e)
        {
            if (display.Text != "")
            {
                if (!display.Text.Contains(","))
                {
                    display.Text += ",";
                }
            }
        }

        /// <summary>
        /// Function handler for click x2 button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void sqr_Click(object sender, EventArgs e)
        {
            if(display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("sqr({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click sqrt button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void sqrt_Click(object sender, EventArgs e)
        {
            if (display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("√({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click logarithm button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void logarithm_Click(object sender, EventArgs e)
        {
            if (display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("log({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click sinnus button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void sinnus_Click(object sender, EventArgs e)
        {
            if (display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("sin({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click cosinnus button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void cosinnus_Click(object sender, EventArgs e)
        {
            if (display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("cos({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click tangens button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void tangens_Click(object sender, EventArgs e)
        {
            if (display.Text == "0")
                isBracketOpen = true;

            elements.Add(String.Format("tan({0})", display.Text));

            displayTop.Text = displayOperations.updateScondLineDisplay(elements);

            currentNumber = 0;

            display.Text = currentNumber.ToString();
        }

        /// <summary>
        /// Function handler for click M+ button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void m_plus_Click(object sender, EventArgs e)
        {
            memoryValue += double.Parse(display.Text);

            if (mc.Enabled == false)
                mc.Enabled = true;

            if (mr.Enabled == false)
                mr.Enabled = true;
        }

        /// <summary>
        /// Function handler for click M- button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void m_minus_Click(object sender, EventArgs e)
        {
            memoryValue -= double.Parse(display.Text);

            if (mc.Enabled == false)
                mc.Enabled = true;

            if (mr.Enabled == false)
                mr.Enabled = true;
        }

        /// <summary>
        /// Function handler for click MR button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void mr_Click(object sender, EventArgs e)
        {
            display.Text = memoryValue.ToString();
            currentNumber = memoryValue;
        }

        /// <summary>
        /// Function handler for click MC button
        /// </summary>
        /// <param name="sender">sender button object</param>
        /// <param name="e">exception</param>
        private void mc_Click(object sender, EventArgs e)
        {
            memoryValue = 0;

            if (mc.Enabled == true)
                mc.Enabled = false;

            if (mr.Enabled == true)
                mr.Enabled = false;
        }
    }
}
