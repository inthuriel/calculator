# Calculator

This project implements idea of virtual calculator with following set of functionalities:  

- basic math operations: `+`,`-`,`*`,`/`  
- advanced math operations: `log`, `sin`, `cos`, `tan`, `sqr`, `sqrt`  
- brackets, percentage  
- memory  

## GUI

![projectui](https://i.ibb.co/yYmR3c5/calculator.jpg)

## Implementation

This software is written in `C#` using `windows forms` and can be compiled at any `x86` processor.  
Software was tested in **Windows 10** environment, fell free to test and use it in other *Windows* distributions (likely **Windows 7** and above).

## Possible improvements

- mathematic notation for to large results  
- interactive keyboard (accept direct key strokes)  
- saving memory value to file  
- unit tests  
